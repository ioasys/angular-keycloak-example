import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  _keycloakService: KeycloakService;
  _keycloakInstance: Keycloak.KeycloakInstance;
  _router: Router

  constructor(
    KeycloakService: KeycloakService,
    Router: Router
  ) {
    this._keycloakService = KeycloakService;
    this._keycloakInstance = KeycloakService.getKeycloakInstance();
    this._router = Router;
  }

  ngOnInit(): void {
    if (this._keycloakInstance.authenticated) {
      this._router.navigate(['/home'])
    } else {
      this._keycloakInstance.login(/*{ redirectUri: 'http://localhost:4200/home' }*/);
    }

  }

}
