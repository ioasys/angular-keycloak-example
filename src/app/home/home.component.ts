import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import * as Keycloak from 'keycloak-js';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  _keycloakService: KeycloakService;
  _keycloakInstance: Keycloak.KeycloakInstance;
  _httpClient: HttpClient;

  constructor(
    KeycloakService: KeycloakService,
    HttpClient: HttpClient
  ) {
    this._keycloakService = KeycloakService;
    this._keycloakInstance = KeycloakService.getKeycloakInstance();
    this._httpClient = HttpClient;
  }

  ngOnInit(): void {
  }

  doAuthRequest = () => this._httpClient.get(
    'https://localhost:7025/WeatherForecast/GetWeatherForecastAuth',
    {
      responseType: 'text'
    }
  ).subscribe(data => {
    console.log(data);
  })

}
